var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SchemaOptions = {
  collection: 'articles',
  versionKey: 'version',
  timestamps: true
};
var SchemaProperties = {
  title:{
    required: true,
    type: String
  },
  content:{
    required: true,
    type: String,
    maxlength: 500
  },
  author:{
    required: true,
    type: String
  },
  comments:{
    type: Array,
    default: []
  }
};
var articleSchema = new Schema(SchemaProperties, SchemaOptions);
module.exports = mongoose.model('article', articleSchema);
