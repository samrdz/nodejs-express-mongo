var mongose = requiere('mongose');
var Schema = mongose.Schema;

var SchemaOptions = {
    collection: 'comments',
    versionKey: 'version',
    timestamps: true
};

var SchemaProperties = {
    article: {
        type: Schema.Types.ObjectID,
        ref: 'Article'
    },
    content: {
        required: true,
        type: String,
        maxlenght: 500
    },
}

autor: {
        required: true,
        type: String,
    },

    postedAt: {
        type: Date,
        default: Date.now
    }
};

var commentsSchema = new Schema(SchemaProperties, SchemaOptions)
module.exports = mongoose.model('Comment', commentsSchema)
