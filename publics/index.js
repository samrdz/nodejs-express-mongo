$(document).ready(function() {
 var getArticlesCall = {
   url: 'http://localhost:3005/articles',
   dataType: 'json',
   method: 'GET'
 };

 $.ajax(getArticlesCall)
   .done(printArticles)
   .fail(logearMensaje)
   .always(notificarUsuario)

 function printArticles(response) {
   var post = $('#article__empity')
     .clone()
     .removeAttr('id')
     .attr('id', article.id)

   $(post)
     .find('.article__content')
       .text(article.content)
       .end()
     .find('.article__title')
       .text(article.title)
       .end()
     .find('.article__author')
       .text(article.author)
       .end()
     .find('.article__link')
       .attr('href', article.link)
       .end();
 }

 function logearMensaje(msg) {
   console.log(msg);
 }
 function notificarUsuario(msg) {
   console.log(msg);
 }

});
