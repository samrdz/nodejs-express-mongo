var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var mongoose = require('mongoose');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
var cors = require('cors');
app.use(cors({origin: 'http://localhost:8888'}));
mongoose.connect('mongodb://192.168.99.100/blog');
app.all('*', function (req, res) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
 res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
});
var route = require('./router');
app.use('/', route);
var server = app.listen(3000, function () {
  console.log('listening on port %s...', server.address().post);
});
