var router = require('express').Router();
var articlesController = require('./controllers/articles');
var articlesController = require('./controllers/comments');
//articles//

router.get('/articles', articlesController.readArticles);
router.post('/articles', articlesController.createArticle);
router.get('/articles/:id', articlesController.readOneArticle);
router.put('/articles/:id', articlesController.updateOneArticle);
router.delete('/articles/:id', articlesController.updateOneArticle);

module.exports = router;

router.get('/comments/:id', articlesController.readOneComments);
router.post('/comments', articlesController.createComments);
router.get('/comments/:id', articlesController.readComments);
router.put('/comments/:id', articlesController.updateOneComments);
router.delete('/comments/:id', articlesController.updateOneComments);

module.exports = router;
