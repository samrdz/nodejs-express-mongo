var router = require('express').Router();
var articlesController = require('./controllers/articles');
router.get('/articles', articlesController.readArticles);
router.post('/articles', articlesController.createArticle);
router.get('/articles/:id', articlesController.readOneArticle);
router.put('/articles/:id', articlesController.updateOneArticle);
router.delete('/articles/:id', articlesController.updateOneArticle);
module.exports = router;
